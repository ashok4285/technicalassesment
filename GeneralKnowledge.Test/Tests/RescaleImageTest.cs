﻿using System.IO;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            string _filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            _filePath = Directory.GetParent(Directory.GetParent(_filePath).FullName).FullName;
            string file = string.Empty;
            var desThumbFilePath = Path.Combine(_filePath, @"Resources\newimag.jpg");
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData("https://picsum.photos/seed/picsum/200/300");

            using (Image images = Image.FromStream(new MemoryStream(bytes)))
            {
                images.Save(Path.Combine(_filePath, @"Resources\car1.jpg"), ImageFormat.Jpeg);
                file = Path.Combine(_filePath, @"Resources\car1.jpg");
            }

            ImageResizer.ImageJob image = new ImageResizer.ImageJob(file, desThumbFilePath, new ImageResizer.ResizeSettings(
              "width= 100;height=80;format=jpg;mode=max"));
            image.CreateParentDirectory = true;
            image.Build();

            var PreviewThumbFilePath = Path.Combine(_filePath, @"Resources\newPreviewimag.jpg");
            ImageResizer.ImageJob imagePreview = new ImageResizer.ImageJob(file, PreviewThumbFilePath, new ImageResizer.ResizeSettings(
              "width= 1200;height=1600;format=jpg;mode=max"));
            imagePreview.CreateParentDirectory = true;
            imagePreview.Build();
        }



    }
}
