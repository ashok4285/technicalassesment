﻿using GeneralKnowledge.Test.App.DataContext;
using GeneralKnowledge.Test.App.DomainModel;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public  void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted
            //read from resource
            //add directly resource 
            ProcessCsv();

        }

        private string GetFile()
        {
            string _filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            _filePath = Directory.GetParent(Directory.GetParent(_filePath).FullName).FullName;
            var csvFile = Path.Combine(_filePath, @"Resources\AssetImport.csv");
            return csvFile;
        }



        private void ProcessCsv()
        {
            try
            {
                string pathToExcelFile = GetFile();
                string sheetName = "AssetImport";
                var excelFile = new ExcelQueryFactory(pathToExcelFile);
                var assetDetails = from a in excelFile.Worksheet<AssetExcel>(sheetName) select a;
                Persist(assetDetails);
                Console.WriteLine();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
        }
        //align to async
        private void Persist(IQueryable<AssetExcel> asetRecords)
        {
            var counter = 0;
            var allDistinctMimeType = asetRecords.Select(x => x.mime_type).Distinct().ToList();
            var allDistinctCountryType = asetRecords.Select(x => x.country).Distinct().ToList();
            using (var ctx = new AssetContext())
            {
                foreach (var mimes in allDistinctMimeType)
                {
                    var mime = new MimeType();
                    mime.Mime_Type = mimes;
                    ctx.MimeTypes.Add(mime);
                }
                foreach (var countries in allDistinctCountryType)
                {
                    var country = new Country();
                    country.CountryName = countries;
                    ctx.Countries.Add(country);
                }
               ctx.SaveChanges();
            }

            using (var ctx = new AssetContext())
            {
                var allMimeTypes = ctx.MimeTypes.ToList();
                var allCountries = ctx.Countries.ToList();
                var assetsDetails = new List<Asset>();
                Parallel.ForEach(asetRecords, (a,state) =>
                {
                    var existingMime = allMimeTypes.Where(x => x.Mime_Type == a.mime_type).FirstOrDefault();
                    var existingCountry = allCountries.Where(x => x.CountryName == a.country).FirstOrDefault();
                    if (existingCountry != null && existingMime != null)
                    {
                        var assets = new Asset();
                        assets.Mime_Type = existingMime;
                        assets.Mime_Id = existingMime.Mime_Id;
                        assets.Description = a.description;
                        assets.FileName = a.file_name;
                        assets.Asset_Id = a.asset_id;
                        assets.Country = existingCountry;
                        assets.CountryId = existingCountry.CountryId;
                        assets.Email = a.email;
                        assets.Created_By = a.created_by;
                        assetsDetails.Add(assets);                      
                    }                
                });               
                try
                {
                    var validAssets = assetsDetails.Where(c => c != null).Distinct().ToList();
                    ctx.Assets.AddRange(validAssets);
                    ctx.SaveChangesAsync();
                    
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }              
            }


         


        }

    }

}
