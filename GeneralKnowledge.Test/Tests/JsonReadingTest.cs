﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using GeneralKnowledge.Test.App.DomainModel;
using System.Linq;
using GeneralKnowledge.Test.App.Domain;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic data retrieval from JSON test
    /// </summary>
    public class JsonReadingTest : ITest
    {
        public string Name { get { return "JSON Reading Test";  } }

        public void Run()
        {
            var jsonData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(jsonData);
        }

        private void PrintOverview(byte[] data)
        {

            var jsonString = System.Text.Encoding.Default.GetString(data).Replace("\r\n","");
            Dictionary<String, Object> values = JsonConvert.DeserializeObject<Dictionary<String, Object>>(jsonString);

            var measurementDatas = JsonConvert.DeserializeObject<MeasurementDetails> (jsonString);

            var measurementData = measurementDatas.Measurements;

            var Temperature = new
            {
                Min = measurementData.Min(t => t.Temperature),
                Max = measurementData.Max(t => t.Temperature),
                Avg = measurementData.Average(t => Convert.ToDouble(t.Temperature))
            };

            var PH = new
            {
                Min = measurementData.Min(t => t.PH),
                Max = measurementData.Max(t => t.PH),
                Avg = measurementData.Average(t => t.PH)
            };

            var Phosphate = new
            {
                Min = measurementData.Min(t => t.Phosphate),
                Max = measurementData.Max(t => t.Phosphate),
                Avg = measurementData.Average(t => Convert.ToDouble(t.Phosphate))
            };

            var Chloride = new
            {
                Min = (from m in measurementData
                       where m.Chloride != null
                       select m).Min(c => c.Chloride),
                Max = measurementData.DefaultIfEmpty().Max(t => t.Chloride),
                Avg = measurementData.DefaultIfEmpty().Average(t => Convert.ToDouble(t.Chloride))
            };

            var Nitrate = new
            {
                Min = measurementData.DefaultIfEmpty().Min(t => t.Nitrate),
                Max = measurementData.DefaultIfEmpty().Max(t => t.Nitrate),
                Avg = measurementData.DefaultIfEmpty().Average(t => Convert.ToDouble(t.Nitrate))
            };

            Console.WriteLine();
            Console.WriteLine("parameter    LOW   AVG   MAX");
            Console.WriteLine("temperature  {0}  {1:N2}  {2}", Temperature.Min, Temperature.Avg, Temperature.Max);
            Console.WriteLine("pH           {0}    {1:N2}   {2}", PH.Min, PH.Avg, PH.Max);
            Console.WriteLine("Chloride     {0}    {1:N2}   {2}", Chloride.Min, Chloride.Avg, Chloride.Max);
            Console.WriteLine("Phosphate    {0}    {1:N2}   {2}", Phosphate.Min, Phosphate.Avg, Phosphate.Max);
            Console.WriteLine("Nitrate      {0}   {1:N2}   {2}", Nitrate.Min, Nitrate.Avg, Nitrate.Max);


        }
    }
}
