﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DomainModel
{
    public class Measurement
    {
        [JsonProperty("temperature")]
        public double Temperature { get; set; }
        [JsonProperty("pH")]
        public int PH { get; set; }
        [JsonProperty("phosphate")]
        public string Phosphate { get; set; }
        [JsonProperty("chloride")]
        public string Chloride { get; set; }
        [JsonProperty("nitrate")]
        public string Nitrate { get; set; }
    }
}
