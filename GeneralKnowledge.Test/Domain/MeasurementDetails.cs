﻿using GeneralKnowledge.Test.App.DomainModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Domain
{
    public class MeasurementDetails
    {
        [JsonProperty("samples")]
        public List<Measurement> Measurements { get; set; }
    }
}
