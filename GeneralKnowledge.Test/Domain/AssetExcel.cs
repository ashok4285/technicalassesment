﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DomainModel
{
    public class AssetExcel
    {       
        public string asset_id { get; set; }
        public string file_name { get; set; }
        public string mime_type { get; set; }
        public string created_by { get; set; }
        public string email { get; set; }
        public string country { get; set; }
        public string description { get; set; }
    }
}
