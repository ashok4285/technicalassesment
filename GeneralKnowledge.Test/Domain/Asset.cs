﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DomainModel
{
    public class Asset
    {        
        [Key]
        public string Asset_Id { get; set; }
        public int Mime_Id { get; set; }        
        public MimeType Mime_Type { get; set; }
        public string FileName { get; set; }
        public string Created_By { get; set; }
        public string Description { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }       
        public string Email { get; set; }

    }
}
