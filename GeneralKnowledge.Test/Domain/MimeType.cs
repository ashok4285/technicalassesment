﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DomainModel
{
    public class MimeType
    {
        [Key]
        public int Mime_Id { get; set; }
        public string Mime_Type { get; set; }
        public ICollection<Asset> Assets { get; set; }
    }
}
