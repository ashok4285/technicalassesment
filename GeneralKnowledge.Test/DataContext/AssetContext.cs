﻿using GeneralKnowledge.Test.App.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace GeneralKnowledge.Test.App.DataContext
{
    public class AssetContext : DbContext
    {
        public AssetContext() : base("name=Default")
        {
           
        }
        public  DbSet<Asset> Assets { get; set; }       
        public  DbSet<MimeType> MimeTypes { get; set; }
        public DbSet<Country> Countries { get; set; }
    }
}
