﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebExperience.Test.App.DomainModel;
using Microsoft.Extensions.Configuration;

namespace WebExperience.Test.Repository
{
    
    public class AssetRepository : DbContext, IAssetRepository
    {
        private readonly AssetContext _context;
        public AssetRepository(AssetContext context)
        {
            _context = context;
        }

        public async Task<string> CreateAsync(Asset AssetDomain)
        {           
                var assetID = Guid.NewGuid().ToString();
                var mimetype = _context.MimeTypes.Where(x => x.Mime_Type == AssetDomain.MimeTypes.Mime_Type).FirstOrDefault();
                var newAsset = new Asset()
                {
                    Created_By = AssetDomain.Created_By,
                    FileName = AssetDomain.FileName,
                    MimeTypes = mimetype,
                    Description = AssetDomain.Description,
                    Countries = AssetDomain.Countries,
                    Email = AssetDomain.Email,
                    Asset_Id = assetID
                };

                if (mimetype == null)
                {
                    newAsset.MimeTypes = new MimeTypes() { Mime_Type = AssetDomain.MimeTypes.Mime_Type };
                }
                _context.Assets.Add(newAsset);
                _context.SaveChangesAsync();
                return newAsset.Asset_Id;   
            
        }

        public async Task<bool> DeleteAsync(string id)
        {
           
            var assetItem = _context.Assets.SingleOrDefault(i => i.Asset_Id == id);
            if (assetItem == null)
            {
                return false;
            }
            _context.Assets.Remove((Asset)assetItem);
            _context.SaveChangesAsync();
            return true;
        }

        public async Task<List<Asset>> GetAsync()
        {                
            return _context.Assets    
                .Include(x => x.MimeTypes)
                .Include(x => x.Countries)
                   .ToList();                   
        }            

        public async Task<string> UpdateAsync(Asset assetItemToUpdate)
        {
            var assetItem = _context.Assets.SingleOrDefault(i => i.Asset_Id == assetItemToUpdate.Asset_Id);
            if (assetItem == null)
            {
                return assetItem.Asset_Id;
            }
            
            var mimetype = _context.MimeTypes.Find(assetItemToUpdate.MimeTypes);
            var country = _context.Countries.Find(assetItemToUpdate.Countries);
            assetItem.Created_By = assetItemToUpdate.Created_By;
            assetItem.Description = assetItemToUpdate.Description;
            assetItem.Email = assetItemToUpdate.Email;
            assetItem.FileName = assetItemToUpdate.FileName;
            assetItem.MimeTypes = mimetype;
            assetItem.Asset_Id = assetItemToUpdate.Asset_Id;
            _context.Assets.Update((Asset)assetItem);
            _context.SaveChangesAsync();
            return assetItem.Asset_Id;
        }
    }
}
