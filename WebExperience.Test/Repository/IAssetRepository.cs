﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebExperience.Test.App.DomainModel;

namespace WebExperience.Test.Repository
{
    public interface IAssetRepository
    {
        Task<List<Asset>> GetAsync();   
        Task<string> UpdateAsync(Asset assetItemToUpdate);
        Task<string> CreateAsync(Asset AssetDomain);
        Task<bool> DeleteAsync(string id);
    }
}
