﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebExperience.Test.App.DomainModel;

using WebExperience.Test.Repository;

namespace WebExperience.Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssetController : ControllerBase
    {

        private readonly IAssetRepository repository;
       

        public AssetController(IAssetRepository repository)
        {
            this.repository = repository;          
        }


        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {            
            var result = await repository.GetAsync();
            return Ok(result);
        }      



        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> Post([FromBody] Asset asset)
        {
            var Asset_Id = await repository.CreateAsync(asset);
            return CreatedAtAction(nameof(Get), new { id = Asset_Id }, null);
        }
       
        
        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Put(string id, [FromBody]Asset assetItemToUpdate)
        {
            var assetItem =await repository.UpdateAsync(assetItemToUpdate);
            if (assetItem == null)
            {
                return NotFound(new { Message = $"AssetItem with id {id} not found" });
            }
            return Ok(new { asset = assetItemToUpdate.Asset_Id });
        }
    
        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(string id)
        {
            var assetItem =await repository.DeleteAsync(id);
            if (assetItem == null)
            {
                return NotFound(new { Message = $"AssetItem with id {id} not found" });
            }

            return NoContent();
        }

    }
}
