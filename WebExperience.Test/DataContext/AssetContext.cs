﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace WebExperience.Test.App.DomainModel
{
    public class AssetContext : DbContext
    {
        public AssetContext(DbContextOptions<AssetContext> options) : base(options)
        {
            
        }
        public DbSet<Asset> Assets { get; set; }       
        public DbSet<MimeTypes> MimeTypes { get; set; }
        public DbSet<Country> Countries { get; set; }
    }
}
