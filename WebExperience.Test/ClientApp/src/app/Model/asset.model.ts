export interface IAssets {
  //id: number;
  asset_Id: string;
  filename: string;
  mimeTypes: IMimeTypes;
  email: string;
  created_by:string;
  description: string;
  countries: ICountry;
}

export interface IMimeTypes {
  //id: number;
  mime_Id: number;
  mime_Type: string; 
}

export interface ICountry {
  //id: number;
  countryID: number;
  countryName: string;
}
