import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetViewModelComponent } from './asset-view-model.component';

describe('AssetViewModelComponent', () => {
  let component: AssetViewModelComponent;
  let fixture: ComponentFixture<AssetViewModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetViewModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetViewModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
