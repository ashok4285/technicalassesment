import { Component, OnInit } from '@angular/core';
import { IAssets, IMimeTypes, ICountry } from '../Model/asset.model';
import { AssetService } from '../asset-services/asset.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { InputTextModule } from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';

@Component({
  selector: 'app-asset-view-model',
  templateUrl: './asset-view-model.component.html',
  styleUrls: ['./asset-view-model.component.css']
})
export class AssetViewModelComponent implements OnInit {
  asset: IAssets[];
  newAssetDetail: IAssets;
  id: number = 0; 
  disabled: boolean = true;
  btneditDisable: boolean = false;
  btnsaveDisable: boolean = true;
  btndelDisable: boolean = false;
  constructor(private service: AssetService, private route: ActivatedRoute,
    private location: Location, private router: Router,
  ) { }

  ngOnInit() {
    debugger;
    this.service.getAssets().subscribe(assetData => {
      this.asset = assetData;      
    });   
  }

  saveAsset(assetDetail) {
    if (assetDetail.asset_Id) {
      assetDetail.asset_Id = "";
      this.service.createAssets(assetDetail).subscribe(id => {
        const userStr = JSON.stringify(id);
        assetDetail.asset_Id = JSON.parse(userStr).id;
        this.asset[0] = assetDetail;
        this.btneditDisable = false;
        this.btnsaveDisable = true;
        this.btndelDisable = false;
      });
      return;
    }

    this.service.updateAssets(assetDetail.asset_Id, assetDetail).subscribe(asset => {
      console.log("Updated" + assetDetail.asset_Id);
      this.btneditDisable = false;
      this.btnsaveDisable = true;
      this.btndelDisable = false;
    });
  }

  deleteAsset(assetDetail) {
    this.service.deleteAssets(assetDetail.asset_Id).subscribe(asset => {
      this.asset = this.asset.filter(item => item.asset_Id !== assetDetail.asset_Id);
      console.log("Deleted");
    });
  }

  editAsset() {
    this.btneditDisable = true;
    this.btndelDisable = true;
    this.btnsaveDisable = false;

  }

  newAsset() {
    this.newAssetDetail = {} as IAssets;
    this.newAssetDetail.mimeTypes = {} as IMimeTypes;
    this.newAssetDetail.mimeTypes.mime_Type = "";
    this.newAssetDetail.countries = {} as ICountry;
    this.newAssetDetail.countries.countryName = "";
    this.asset.unshift(this.newAssetDetail);
    this.btneditDisable = true;
    this.btndelDisable = true;
    this.btnsaveDisable = false;
  }  

 
}
