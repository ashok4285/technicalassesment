import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IAssets } from '../Model/asset.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class AssetService {
  httpport: string = "https://localhost:44334";
  constructor(private httpClient: HttpClient) {
  }
  
  getAssets(): Observable<IAssets[]> {
    let url = this.httpport + '/api/Asset';
    return this.httpClient.get<IAssets[]>(url);
  }

  getAssetCount(): Observable<number> {    
    let url = this.httpport +'/api/Asset/getpagecount';
    return this.httpClient.get<number>(url);
  } 

  updateAssets(id: string, assetData: IAssets): Observable<IAssets> {
    let url = this.httpport +`/api/Asset/` + id;
    return this.httpClient.put<IAssets>(url, assetData, httpOptions);
  }
  
  createAssets(assetData: IAssets): Observable<IAssets> {
    let url = this.httpport +`/api/Asset`
    return this.httpClient.post<IAssets>(url, assetData, httpOptions);
  }

  deleteAssets(id: string): Observable<IAssets> {
    let url = this.httpport +`/api/Asset/`+ id;
    return this.httpClient.delete<IAssets>(url,httpOptions);
  }


}
