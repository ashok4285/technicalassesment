import { TestBed, inject } from '@angular/core/testing';

import { AssetServiceService } from './asset-service.service';

describe('AssetServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AssetServiceService]
    });
  });

  it('should be created', inject([AssetServiceService], (service: AssetServiceService) => {
    expect(service).toBeTruthy();
  }));
});
