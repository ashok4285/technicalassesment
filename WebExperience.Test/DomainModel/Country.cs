﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebExperience.Test.App.DomainModel
{
    public class Country
    {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        [JsonIgnore]
        public ICollection<Asset> Assets { get; set; }
    }
}
