﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebExperience.Test.App.DomainModel
{
    public class Asset
    {       
        [Key]       
        public string Asset_Id { get; set; }
        public int Mime_Id { get; set; }                   
        public MimeTypes MimeTypes { get; set; }
        public string FileName { get; set; }       
        public string Created_By { get; set; }       
        public string Description { get; set; }      
        public int CountryID { get; set; }
        public Country Countries { get; set; }
        public string Email { get; set; }

    }
}
