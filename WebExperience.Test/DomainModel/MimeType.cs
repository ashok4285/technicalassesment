﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WebExperience.Test.App.DomainModel
{
    public class MimeTypes
    {
        [Key]
        public int Mime_Id { get; set; }
        public string Mime_Type { get; set; }
        [JsonIgnore]
        public ICollection<Asset> Assets { get; set; }
    }
}
